﻿using System.Collections.Generic;

namespace Littlespace.Chemistry
{
    public class Molecule
    {
        private List<Atom> atoms;

        public List<Atom> atomsList
        {
            get { return atoms; }
            set { atoms = value; }
        }

        public Molecule(string name)
        {
            Name = name;
            atoms = new List<Atom>();
        }

        public int AtomsCount
        {
            get { return atoms.Count; }
        }

        public string Formula //myślę, że ta nazwa jest bardziej trafna i nie będzie się mylić z symbolami atomów
        {
            get
            {
                string formula = "";
                foreach (Atom atom in atoms)
                {
                    formula += atom.Symbol.ToString();
                }

                return formula;
            }
        }

        public string Name { get; private set; }

        public float Mass
        {
            get
            {
                float mass = 0f;
                foreach (Atom atom in atoms)
                {
                    mass += atom.Mass;
                }

                return mass;
            }
        }

        public bool IsCompound
        {
            get
            {
                if (atoms.Count <= 1)
                    return false;

                string firstSymbol = (atoms[0].Symbol.ToString());

                for (int i = 1; i < atoms.Count; i++)
                {
                    if (firstSymbol != atoms[i].Symbol.ToString())
                        return true;
                }

                return false;
            }
        }

        public void AddAtoms(AtomSymbol symbol, int numberOfAtoms) 
        {
            for (int i = 0; i < numberOfAtoms; i++)
            {
                atomsList.Add(Atom.CreateAtom(symbol));
            }
        }
    }
}
