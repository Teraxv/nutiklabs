﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Littlespace.Chemistry
{
    public class Atom
    {
        public string Name { get; set; }
        public AtomSymbol Symbol { get; set; }
        public int AtomicNumber { get; private set; }
        public int Group { get; private set; }
        public int Period { get; private set; }
        public float Mass { get; private set; }

        public bool IsMetal
        {
            get
            {
                switch (Group)
                {
                    case 1:
                        return Period != 1;
                    case 13:
                        return Period != 2;
                    case 14:
                        return Period > 4;
                    case 15:
                    case 16:
                        return Period > 5;
                    case 17:
                    case 18:
                        return Period > 6;
                    default:
                        return true;
                }
            }
        }
        
        private Atom(string name, AtomSymbol symbol, int atomicNumber, int group, int period, float mass)
        {
            Name = name;
            Symbol = symbol; 
            AtomicNumber = atomicNumber;
            Group = group;
            Period = period;
            Mass = mass;
        }
        
        public static Atom CreateAtom(AtomSymbol symbol)
        {
            switch (symbol)
            {
                case AtomSymbol.H: 
                    return new Atom("hydrogen", symbol, 1, 1, 1, 1.0f);
                case AtomSymbol.C:
                    return new Atom("carbon", symbol, 6, 14, 2, 12.0f);
                case AtomSymbol.N:
                    return new Atom("nitrogen", symbol, 7, 15, 2, 14.00f);
                case AtomSymbol.O:
                    return new Atom("oxygen", symbol, 8, 16, 2, 15.99f);
                case AtomSymbol.F:
                    return new Atom("fluorine", symbol, 9, 17, 2, 18.99f);
                case AtomSymbol.Na:
                    return new Atom("sodium", symbol, 11, 1, 3, 22.99f);
                case AtomSymbol.Cl:
                    return new Atom("chlorine", symbol, 17, 17, 3, 35.45f);
                default:
                    throw new ArgumentException("Unknown atom symbol");
            }
        }
    }
}
