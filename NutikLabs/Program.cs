﻿using Littlespace.Chemistry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Littlespace
{
    class Program
    {
        static void Main(string[] args)
        {
            var toxicityDictionary = new Dictionary<Molecule, bool>();

            Console.WriteLine("How many molecules would you like to create?");
            string _numberOfMolecules = Console.ReadLine();
            int numberOfMolecules = Int32.Parse(_numberOfMolecules);

            for (int i = 0; i < numberOfMolecules; i++)
            {
                Molecule newMolecule = CreateMolecule();
                AddAtomsToNewMolecule(newMolecule);
                AddNewMoleculeToDictionary(newMolecule, toxicityDictionary);
            }
            
            Console.WriteLine("Molecules that are not toxic:");
            foreach (var pair in toxicityDictionary)
            {
                if (!pair.Value)
                {
                    Console.WriteLine(pair.Key.Name);
                    Console.WriteLine(" formula: " + pair.Key.Formula);
                    Console.WriteLine(" mass: " + pair.Key.Mass);
                }
            }

            for (int i = toxicityDictionary.Count - 1; i >= 0; i--)
            {
                var itemKey = toxicityDictionary.ElementAt(i).Key;

                if (!itemKey.IsCompound)
                    toxicityDictionary.Remove(itemKey);
            }

            Console.WriteLine();
            Console.WriteLine("Molecules that are compounds:");
            foreach (var kv in toxicityDictionary)
            {
                Console.WriteLine(kv.Key.Name);
            }
        }

        public static Molecule CreateMolecule()
        {
            Console.WriteLine("Please write a name of the molecule you want to create: ");
            string name = Console.ReadLine();
            return new Molecule(name);
        }

        public static void AddAtomsToNewMolecule(Molecule molecule)
        {
            Console.WriteLine("Please specify the number of chemical elements in the molecule:");
            string _numberOfElements = Console.ReadLine();
            int numberOfElements = Int32.Parse(_numberOfElements);

            for (int i = 0; i < numberOfElements; i++)
            {
                Console.WriteLine("Please specify the symbol of an element number {0} out of {1}", i+1, numberOfElements);
                string _elementSymbol = Console.ReadLine();
                var elementSymbol = (AtomSymbol)Enum.Parse(typeof(AtomSymbol), _elementSymbol);

                Console.WriteLine("Please specify the number of {0} atoms:", _elementSymbol);
                string _numberOfAtoms = Console.ReadLine();
                int numberOfAtoms = Int32.Parse(_numberOfAtoms);

                molecule.AddAtoms(elementSymbol, numberOfAtoms);
            }
        }

        public static void AddNewMoleculeToDictionary(Molecule molecule, Dictionary<Molecule, bool> dictionary)
        {
            Console.WriteLine("Is {0} toxic? Write 'yes' or 'no'", molecule.Name);
            string _toxicity = Console.ReadLine();
            bool toxicity;

            if (_toxicity == "yes")
                toxicity = true;
            else 
                toxicity = false;

            dictionary.Add(molecule, toxicity);
        }
    }
}
